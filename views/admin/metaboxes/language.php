<div class="ckrs-metabox">

    <section>
        <aside>
            <label for="<?= $metabox["id"] ?>[title]">
                Titel
            </label>
        </aside>
        <main>
            <input type="text" name="<?= $metabox["id"] ?>[title]" value="<?= $title ?>">
        </main>
    </section>

    <section>
        <aside>
            <label for="<?= $metabox["id"] ?>[content]">
                Inhalt
            </label>
        </aside>
        <main>
			<?php wp_editor(
				$content,
				$metabox["id"] . "_content",
				[
					"textarea_name" => $metabox["id"] . "[content]",
                    "textarea_rows" => 10
				] ); ?>
        </main>
    </section>

</div>