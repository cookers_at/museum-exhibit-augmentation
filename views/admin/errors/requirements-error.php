<div class="error">

    <p>Museum Exhibit Augmentation error: Your environment doesn't meet all of the system requirements listed below.</p>

    <ul class="ul-disc">

        <li>
            <strong>PHP <?php echo CKRS_MEA_REQUIRED_PHP_VERSION; ?>+ is required</strong>
            <em>(You're running version <?php echo PHP_VERSION; ?>)</em>
        </li>

        <li>
            <strong>WordPress <?php echo CKRS_MEA_REQUIRED_WP_VERSION; ?>+ is required</strong>
            <em>(You're running version <?php echo esc_html( $wp_version ); ?>)</em>
        </li>

    </ul>

</div>