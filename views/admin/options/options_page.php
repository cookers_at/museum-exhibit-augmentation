<div>
	<h2>Einstellungen für Zusatzinformationen</h2>
	<form method="post" action="options.php">
		<?php settings_fields( $this->option_group ); ?>

		<br/><hr/><br/><!-- --------------------------------------------------- -->

		<h3>API Optionen</h3>

		<table>
			<?php $this->display_options_fields(); ?>
		</table>

		<?php submit_button(); ?>
	</form>
</div>