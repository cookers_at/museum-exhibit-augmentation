 * Plugin Name: Museum Exhibit Augmentation
 * Plugin URI: https://cookers.at/development/museum-exhibit-augmentation/
 * Description: Augmentation plugin for museum exhibitions. Using custom alphanumeric and QR codes.
 * Version: 0.1.0
 * Author: Gerhard Kocher
 * Author URI: https://cookers.at