<?php
/**
 * Plugin Name: Museum Exhibit Augmentation
 * Plugin URI: https://cookers.at/development/museum-exhibit-augmentation/
 * Description: Augmentation plugin for museum exhibitions. Using custom alphanumeric and QR codes.
 * Version: 0.1.0
 * Author: Gerhard Kocher
 * Author URI: https://cookers.at
 **/

namespace at\cookers\wp\mea;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


define( 'CKRS_MEA_REQUIRED_PHP_VERSION', '5.6' );
define( 'CKRS_MEA_REQUIRED_WP_VERSION', '3.0' );

define( 'CKRS_MEA_DIR', plugin_dir_path( __FILE__ ) );
define( 'CKRS_MEA_URL', plugin_dir_url( __FILE__ ) );
define( 'CKRS_MEA_PLUGIN_BASENAME', dirname( plugin_basename( __FILE__ ) ) );

define( 'CKRS_MEA_AUGMENT_POSTTYPE', 'ckrs-augmentation' );
define( 'CKRS_MEA_I18N_DOMAIN', 'ckrs-museum-exhibit-augmentation' );



/**
 * Checks the requirements.
 *
 * @return bool <code>true</code> if system requirements are met, <code>false</code> otherwise.
 */
function ckrs_mea_check_requirements() {
	global $wp_version;

	if ( version_compare( PHP_VERSION, CKRS_MEA_REQUIRED_PHP_VERSION, '<' ) ) {
		return false;
	}
	if ( version_compare( $wp_version, CKRS_MEA_REQUIRED_WP_VERSION, '<' ) ) {
		return false;
	}

	return true;
}

/**
 * Prints an error that the system requirements weren't met.
 */
function ckrs_mea_show_requirements_error() {
	global $wp_version;
	require_once( CKRS_MEA_DIR . '/views/admin/errors/requirements-error.php' );
}

/**
 * Begins execution of the plugin.
 */
function run_ckrs_mea() {
	/**
	 * Check requirements and load main class
	 * The main program needs to be in a separate file that only gets loaded if the plugin requirements are met.
	 * Otherwise older PHP installations could crash when trying to parse it.
	 **/
	if ( ckrs_mea_check_requirements() ) {
		/**
		 * The core plugin class that is used to define internationalization,
		 * admin-specific hooks, and public-facing site hooks.
		 */
		require_once CKRS_MEA_DIR . "classes/Setup.php";

		/**
		 * Begins execution of the plugin.
		 *
		 * Since everything within the plugin is registered via hooks,
		 * then kicking off the plugin from this point in the file does
		 * not affect the page life cycle.
		 */
		$plugin = Setup::get_instance();
	} else {
		add_action( 'admin_notices', 'ckrs_mea_show_requirements_error' );
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		deactivate_plugins( plugin_basename( __FILE__ ) );

	}
}

run_ckrs_mea();