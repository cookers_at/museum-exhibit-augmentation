<?php


namespace at\cookers\wp\mea;


class Augment_Options {
	private $option_group = CKRS_MEA_AUGMENT_POSTTYPE."-options";

	private $options_list = [
						"ckrs_mea_qrcode_url" => "QR-Code Frontend URL",
					];
	private $language_options_list = [
						"error_title" => '"Fehler" Überschrift',
						"error_not_found" => 'Fehler "Inhalt nicht gefunden"',
						"error_other_languages" => 'Fehler "nicht in Sprache verfügbar"'
					];



	public function __construct() {
		add_action( 'admin_init', [ $this, 'register_settings' ] );
		add_action( 'admin_menu', [ $this, 'add_options_page' ], 9);
	}


	public function register_settings() {
		foreach ( $this->options_list as $key => $value ) {
			add_option( $key, '');
			register_setting( $this->option_group, $key);
		}

		$display_languages = Augment_Post_Type::get_available_languages();
		$display_languages["general"] = "OHNE SPRACHE";
		foreach ( $this->language_options_list as $key => $value ) {
			foreach ( $display_languages as $language_code => $language_text ) {
				$setting = $key.'_'.$language_code;
				add_option( $setting, '' );
				register_setting( $this->option_group, $setting );
			}
		}
	}


	public function add_options_page() {
		add_submenu_page(
			'edit.php?post_type='.CKRS_MEA_AUGMENT_POSTTYPE,
			__( 'Einstellungen für Zusatzinformationen', CKRS_MEA_I18N_DOMAIN ),
			__( 'Einstellungen', CKRS_MEA_I18N_DOMAIN ),
			'manage_options',
			'settings',
			[ $this, 'display_options_page' ] );
	}


	public function display_options_page() {
		require_once( CKRS_MEA_DIR . 'views/admin/options/options_page.php' );
	}

	public function display_options_fields() {
		foreach ($this->options_list as $option => $title) {
			$this->render_options_field($title, $option, get_option($option));
		}
		$display_languages = Augment_Post_Type::get_available_languages();
		$display_languages["general"] = "OHNE SPRACHE";
		foreach ( $this->language_options_list as $key => $title ) {
			foreach ( $display_languages as $language_code => $language_text ) {
				$option = $key.'_'.$language_code;
				$this->render_options_field("$title ($language_text)", $option, get_option($option));
			}
		}
	}

	private function render_options_field($title, $key, $value) {
		require( CKRS_MEA_DIR . 'views/admin/options/field.php' );
	}
}