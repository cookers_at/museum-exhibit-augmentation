<?php


namespace at\cookers\wp\mea;


class Augment_DAO {
	public function __construct() {
		add_action( 'wp_ajax_nopriv_augment_dao', [ $this, 'get_ajax' ] );
		add_action( 'wp_ajax_augment_dao', [ $this, 'get_ajax' ] );
		add_filter( 'allowed_http_origins', [ $this, 'add_allowed_origins' ] );
	}

	private function get_available_languages( $post_id ) {
		$available = [];
		foreach ( Augment_Post_Type::get_available_languages() as $language_code => $language_text ) {
			if ( get_post_meta( $post_id, Augment_Post_Type::get_meta_key_from_language( $language_code ), true ) ) {
				$available[$language_code] = $language_text;
			}
		}

		return $available;
	}

	private function get_error( $error, $language = null ) {
		return get_option( $error . "_" . $language ) ?: get_option( $error . "_general" ) ?: get_option( $error . "_de" );
	}

	function get_ajax() {
		header( 'Content-Type: application/json; charset=UTF8' );

		$slug     = $_REQUEST["slug"];
		$language = $_REQUEST["language"];

		if ( $post = get_page_by_path( $slug, OBJECT, CKRS_MEA_AUGMENT_POSTTYPE ) ) {
			$post_id = $post->ID;
		}

		if ( isset( $post_id ) ) {
			if ( isset( $language ) && ! empty( $language ) ) {
				echo json_encode( $this->get_language_text( $post_id, $language ) );
			} else {
				echo json_encode( $this->get_available_data( $post_id ) );
			}
		} else {
			$language = $language ?? "de"; // default language

			echo json_encode( [
				                  "title" => $this->get_error( "error_title", $language ),
				                  "error" => $this->get_error( "error_not_found", $language ),
			                  ] );
		}

		wp_die();
	}

	function get_available_data( $post_id ) {
		$data = [
			"title"     => get_the_title( $post_id ),
			"type"      => "translation", // TODO: "media"
			"languages" => $this->get_available_languages( $post_id )
		];

		return $data;
	}

	function get_language_text( $post_id, $language ) {
		$value = get_post_meta( $post_id, Augment_Post_Type::get_meta_key_from_language( $language ), true );
		$value = base64_decode( $value );
		$value = unserialize( $value );

		$title   = $value["title"] ?? "";
		$content = html_entity_decode( stripslashes( $value["content"] ) ) ?? "";

		if ( $content ) {
			return [
				"title"   => $title ?? get_the_title( $post_id ),
				"content" => $content
			];
		} else {
			if ( $available_languages = $this->get_available_languages( $post_id ) ) {
				return [
					"title"           => $this->get_error( "error_title", $language ),
					"error"           => $this->get_error( "error_other_languages", $language ),
					"other_languages" => $available_languages
				];
			} else {
				return [
					"title" => $this->get_error( "error_title", $language ),
					"error" => $this->get_error( "error_not_found", $language )
				];
			}
		}
	}

	function add_allowed_origins( $origins ) {
		$origins[] = 'https://qr.museumsportal.com';
		$origins[] = 'https://qr.museumsportal.alpha.cserver.biz';
		$origins[] = 'https://aurora.museumsportal.local';

		return $origins;
	}
}