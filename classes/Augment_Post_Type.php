<?php


namespace at\cookers\wp\mea;


class Augment_Post_Type {
	public static function get_available_languages() {
		return [
			'de' => 'Deutsch',
			'en' => 'English',
			'it' => 'Italiano',
		];
	}

	public static function get_meta_key_from_language( $language ) {
		if ( strlen( $language ) != 2 ) {
			throw new \InvalidArgumentException( "Language must be in Alpha-2 code format." );
		}

		return "augment_lang_$language";
	}

	public function add_wp_actions() {
		add_action( 'init', [ $this, 'add_post_type' ] );
		add_action( 'admin_init', [ $this, 'add_post_meta_boxes' ] );
		add_action( 'save_post', [ $this, 'save_meta_box_language' ], 1, 2 );
		add_action( 'admin_enqueue_scripts', [ $this, 'add_admin_style' ] );
		add_filter( 'wp_unique_post_slug', [ $this, 'custom_unique_post_slug' ], 10, 4 );
		add_filter( 'manage_'.CKRS_MEA_AUGMENT_POSTTYPE.'_posts_columns', [ $this, 'add_slug_column' ] );
		add_action( 'manage_'.CKRS_MEA_AUGMENT_POSTTYPE.'_posts_custom_column', [ $this, 'slug_column' ], 10, 2);
	}


	/**
	 * Add the custom post type.
	 */
	public function add_post_type() {

		$labels   = array(
			'name'               => __( 'Zusatzinformationen', CKRS_MEA_I18N_DOMAIN ),
			'singular_name'      => __( 'Zusatzinformation', CKRS_MEA_I18N_DOMAIN ),
			'add_new'            => __( 'Neue Zusatzinformation', CKRS_MEA_I18N_DOMAIN ),
			'add_new_item'       => __( 'Neue Zusatzinformation', CKRS_MEA_I18N_DOMAIN ),
			'edit_item'          => __( 'Zusatzinformation ändern', CKRS_MEA_I18N_DOMAIN ),
			'new_item'           => __( 'Neue Zusatzinformation', CKRS_MEA_I18N_DOMAIN ),
			'view_item'          => __( 'Zusatzinformation ansehen', CKRS_MEA_I18N_DOMAIN ),
			'search_items'       => __( 'Zusatzinformation suchen', CKRS_MEA_I18N_DOMAIN ),
			'not_found'          => __( 'Keine Zusatzinformation gefunden', CKRS_MEA_I18N_DOMAIN ),
			'not_found_in_trash' => __( 'Keine Zusatzinformation im Papierkorb', CKRS_MEA_I18N_DOMAIN )
		);
		$supports = array(
			'editor',
			'title',
			//			'revisions'
		);
		$args     = array(
			'labels'              => $labels,
			'supports'            => $supports,
			'rewrite'             => [ 'slug' => 'qrcode' ],
			//			'show_in_rest'        => false, // Gutenberg
			'public'              => true,
			'has_archive'         => true,
			'exclude_from_search' => true,
			'menu_position'       => 40,
			'menu_icon'           => 'dashicons-media-document',
		);
		register_post_type( CKRS_MEA_AUGMENT_POSTTYPE, $args );

	}


	/**
	 * Generate random slug for the code.
	 *
	 * @param $slug
	 * @param $post_ID
	 * @param $post_status
	 * @param $post_type
	 *
	 * @return string: the slug
	 */
	function custom_unique_post_slug( $slug, $post_id, $post_status, $post_type ) {
		if ( CKRS_MEA_AUGMENT_POSTTYPE == $post_type ) {
			$post = get_post( $post_id );
			if ( empty( $post->post_name ) || $slug != $post->post_name ) {
				while(get_page_by_path($slug, OBJECT, CKRS_MEA_AUGMENT_POSTTYPE)) {
					$codeInput = rand( 100, 999 ) . $post_id;
					$slug      = $this->convBase( $codeInput, "abcdefhikmnprstuvwxyz123456789" );
				}
			}
		}

		return $slug;
	}

	/**
	 * Base conversion function, from PHP.net documentation comments, adapted.
	 * @param $base10: the input number (in base10)
	 * @param $toBaseInput: the target base with characters.
	 *
	 * @return string
	 */
	private function convBase( $base10, $toBaseInput ) {
		$toBase    = str_split( $toBaseInput, 1 );
		$toLen     = strlen( $toBaseInput );
		$retval    = '';
		if ( $base10 < strlen( $toBaseInput ) ) {
			return $toBase[ $base10 ];
		}
		while ( $base10 != '0' ) {
			$retval = $toBase[ bcmod( $base10, $toLen ) ] . $retval;
			$base10 = bcdiv( $base10, $toLen, 0 );
		}

		return $retval;
	}


	/**
	 * Add custom column to backend list view.
	 *
	 * @param $columns
	 * @return mixed
	 */
	function add_slug_column( $columns ) {
		$columns['code'] = "Code";
		return $columns;
	}

	/**
	 * Show the slug/code in custom column.
	 *
	 * @param $column
	 * @param $post_id
	 */
	function slug_column( $column, $post_id ) {
		if ('code' === $column) {
			$post = get_post( $post_id );
			$url = get_option( "ckrs_mea_qrcode_url" );

			echo "<a href=\"$url/{$post->post_name}\">{$post->post_name}</a>";
		}
	}


	/**
	 * Add the post meta boxes
	 *
	 * see https://developer.wordpress.org/reference/functions/add_meta_box for a full explanation of each property
	 */
	function add_post_meta_boxes() {
		foreach ( self::get_available_languages() as $code => $language ) {
			add_meta_box(
				self::get_meta_key_from_language( $code ), // div id containing rendered fields
				$language, // section heading displayed as text
				[ $this, "post_meta_box_language" ], // callback function to render fields
				CKRS_MEA_AUGMENT_POSTTYPE, // name of post type on which to render fields
				"normal", // location on the screen
				"low", // placement priority
				[
					"code" => $code
				]
			);
		}

		add_meta_box(
			"augment_qrcode", // div id containing rendered fields
			"QR Code", // section heading displayed as text
			[ $this, "post_meta_box_qrcode" ], // callback function to render fields
			CKRS_MEA_AUGMENT_POSTTYPE, // name of post type on which to render fields
			"side", // location on the screen
			"low" // placement priority
		);
	}

	function post_meta_box_language( $post, $metabox ) {
		$value = get_post_meta( $post->ID, $metabox["id"], true );
		$value = base64_decode( $value );
		$value = unserialize( $value );

		$title   = $value["title"] ?? "";
		$content = html_entity_decode( stripslashes( $value["content"] ?? "" ) );

		wp_nonce_field( basename( __FILE__ ), $metabox["id"] . '_nonce' );

		require( CKRS_MEA_DIR . 'views/admin/metaboxes/language.php' );
	}

	function post_meta_box_qrcode( $post ) {
		$code       = $post->post_name;
		$qrcode_url = get_option( "ckrs_mea_qrcode_url" );
		$target     = rtrim( $qrcode_url, "/" ) . '/' . $code;

		require( CKRS_MEA_DIR . 'views/admin/metaboxes/qrcode.php' );
	}


	function save_meta_box_language( $post_id ) {
		error_reporting(0);
		// check autosave
		if ( wp_is_post_autosave( $post_id ) ) {
			return 'autosave';
		}

		//check post revision
		if ( wp_is_post_revision( $post_id ) ) {
			return 'revision';
		}

		// check permissions
		if ( CKRS_MEA_AUGMENT_POSTTYPE == $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return 'cannot edit page';
			}
		} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
			return 'cannot edit post';
		}

		//so our basic checking is done
		foreach ( self::get_available_languages() as $code => $language ) {
			$key     = self::get_meta_key_from_language( $code );
			$title   = $_POST[ $key ]["title"];
			$content = $_POST[ $key ]["content"];

			$value = serialize( [
				                    "title"   => $title,
				                    "content" => htmlentities( wpautop( $content ) )
			                    ] );

			if ( ! isset( $_POST[ $key . '_nonce' ] ) || ! wp_verify_nonce( $_POST[ $key . '_nonce' ], basename( __FILE__ ) ) ) {
				return 'nonce not verified';
			}

			if ( ! $_POST[ $key ]["title"] && ! $_POST[ $key ]["content"] ) {
				delete_post_meta( $post_id, $key );
			} else {
				update_post_meta( $post_id, $key, base64_encode( $value ) );
			}
		}
	}


	function add_admin_style() {
		wp_enqueue_style( 'ckrs-metaboxes-styles', CKRS_MEA_URL . 'views/admin/metaboxes/metaboxes.css' );
	}
}