<?php


namespace at\cookers\wp\mea;


if ( ! class_exists( __NAMESPACE__ . '\Setup' ) ) {

	class Setup {

		/**
		 *
		 * @access   private
		 * @var      Setup $instance Instance of this class.
		 */
		private static $instance;

		/**
		 * Provides access to a single instance of a module using the singleton pattern
		 *
		 * @return object
		 */
		public static function get_instance() {

			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;

		}

		/**
		 * Constructor
		 */
		protected function __construct() {
			$this->load_libraries();
			spl_autoload_register( array( $this, 'load_dependencies' ) );

			$augment_post_type = new Augment_Post_Type();
			$augment_post_type->add_wp_actions();

			$augment_options_page = new Augment_Options();

			$augment_dao = new Augment_DAO();
			$augment_dao;

		}

		/**
		 * Loads libraries.
		 *
		 * - [none]
		 */
		private function load_libraries() {

		}

		/**
		 * Loads all Plugin dependencies
		 */
		private function load_dependencies( $class ) {
		if ( false !== strpos( $class, __NAMESPACE__ ) ) {
			$class = str_replace( __NAMESPACE__ . '\\', '', $class );

			//  $class_file_name = str_replace('_', '-', strtolower($class)) . '.php';
			$class_file_name = $class . '.php';
			$folder          = '/';

			if ( false !== strpos( $class, '_Admin' ) ) {
				$folder .= 'admin/';
			}

			if ( false !== strpos( $class, 'Controller' ) ) {
				$path = CKRS_MEA_DIR . 'controllers' . $folder . $class_file_name;
				require_once( $path );
				//				} elseif ( file_exists( CKRS_MEA_DIR . 'models' . $folder . $class_file_name ) ) {
				//					$path = CKRS_MEA::get_plugin_path() . 'models' . $folder . $class_file_name;
				//					require_once( $path );
			} else {
				$path = CKRS_MEA_DIR . 'classes/' . $class_file_name;
				require_once( $path );
			}
		}
	}


		/**
		 * Prepares sites to use the plugin during single or network-wide activation
		 *
		 * @param bool $network_wide
		 */
		public function activate( $network_wide ) {

		}

		/**
		 * Rolls back activation procedures when de-activating the plugin
		 */
		public function deactivate() {

			return true;

		}

		/**
		 * Fired when user uninstalls the plugin, called in uninstall.php file
		 */
		public static function uninstall_plugin() {

			return true;

		}

	}

}